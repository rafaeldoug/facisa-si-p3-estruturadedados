/**
 * 
 */
package br.cesed.si.p3;

import br.cesed.si.p3.exceptions.ElementoNaoEncontradoException;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
public class Deque<T> extends Lista {

	private T objeto;

	/**
	 * 
	 */
	public Deque() {
	}

	/**
	 * @param objeto
	 */
	public Deque(T objeto) {
		this.objeto = objeto;
	}

	/**
	 * @param objeto
	 */
	public void insertFirst(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel adicionar valores nulos.");
		}

		if (isEmpty()) {
			inicio = fim = new Node(objeto);
		} else {
			inicio = new Node(objeto, inicio);
		}
		inseridos++;
	}

	/**
	 * @param objeto
	 */
	public void insertLast(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel adicionar valores nulos.");
		}

		if (isEmpty()) {
			inicio = fim = new Node(objeto);
		} else {
			Node aux = fim;
			fim = new Node(objeto);
			aux.setProxNode(fim);
		}

		inseridos++;
	}

	/**
	 * @throws ListaVaziaException
	 * 
	 */
	public void removeFirst() throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		} else {
			inicio = inicio.getProxNode();
		}
		inseridos--;
	}

	public void removeLast() throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		} else if (inicio == fim) {
			inicio = fim = null;
		} else {
			Node aux = inicio;
			while (aux.getProxNode() != fim) {
				aux = aux.getProxNode();
			}
			fim = aux;
			aux.setProxNode(null);
		}
		inseridos--;
	}

	public void removeByValue(T objeto) throws ListaVaziaException, ElementoNaoEncontradoException {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel remover valores nulos.");
		}

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		if (inicio.getObjeto().equals(objeto) && fim.getObjeto().equals(objeto)) {

			inicio = fim = null;

		} else if (inicio.getObjeto().equals(objeto)) {

			inicio = inicio.getProxNode();

		} else if (size() == 2 && fim.getObjeto().equals(objeto)) {

			inicio.setProxNode(null);
			fim = inicio;

		} else {

			boolean encontrado = false;

			Node aux = inicio;
			Node atual = aux;
			for (int i = 0; i < inseridos && encontrado == false; i++) {
				if (aux.getObjeto().equals(objeto)) {
					encontrado = true;
				}
				atual = aux;
				aux = aux.getProxNode();
			}
			if (encontrado == false) {
				throw new ElementoNaoEncontradoException("Elemento n�o encontrado!");
			} else {
				atual.setProxNode(aux.getProxNode());
				aux.setProxNode(null);
			}
		}
		inseridos--;
	}

	public void removeByIndex(int index) throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		if (inseridos <= index) {
			throw new ArrayIndexOutOfBoundsException("Insira uma posi��o v�lida!");
		}
		if (index < 0) {
			throw new NumberFormatException("N�o s�o permitios valores negativos!");
		}

		if (size() == 1 && index == 0) { // tratamento para uma lista tamanho 1
			inicio = fim = null;
		} else if (size() == 2) { // tratamento para uma lista tamanho 2
			if (index == 0) {
				inicio = fim;
			} else if (index == 1) {
				fim = inicio;
			}
		} else {

			Node aux = inicio;
			Node atual = aux;
			int pos = 0;
			while (pos < index) {
				atual = aux;
				aux = aux.getProxNode();
				pos++;
			}
			atual.setProxNode(aux.getProxNode());
			aux.setProxNode(null);
		}
		inseridos--;

	}

	/**
	 * @return the objeto
	 */
	public T getObjeto() {
		return objeto;
	}

}
