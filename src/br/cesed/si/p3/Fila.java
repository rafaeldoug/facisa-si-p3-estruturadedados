/**
 * 
 */
package br.cesed.si.p3;

import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
public class Fila<T> extends Lista {

	private T objeto;

	/**
	 * @param objeto
	 */
	public Fila() {
	}

	/**
	 * @param objeto
	 */
	public Fila(T objeto) {
		this.objeto = objeto;
	}

	/**
	 * Adiciona elemento ao inicio da fila
	 * 
	 * @param objeto
	 */
	public void enqueue(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel adicionar valores nulos.");
		}

		if (isEmpty()) {
			inicio = fim = new Node(objeto);
		} else {
			Node aux = inicio;
			while (aux.getProxNode() != null) {
				aux = aux.getProxNode();
			}
			fim = new Node(objeto);
			aux.setProxNode(fim);
		}
		inseridos++;

	}

	/**
	 * Retorna o valor do primeiro elemento da lista e o remove
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	public Object dequeue() throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		Node aux = inicio;
		inicio = inicio.getProxNode();
		inseridos--;

		return aux.getObjeto();
	}

	/**
	 * Retorna primeiro elemento da fila
	 * 
	 * @return
	 * @throws ListaVaziaException 
	 */
	public Object front() throws ListaVaziaException {
		
		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		return inicio.getObjeto();
	}

	/**
	 * @return the objeto
	 */
	public T getObjeto() {
		return objeto;
	}

}
