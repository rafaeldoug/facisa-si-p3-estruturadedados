/**
 * 
 */
package br.cesed.si.p3.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.Deque;
import br.cesed.si.p3.entities.Conta;
import br.cesed.si.p3.exceptions.ElementoNaoEncontradoException;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
class DequeTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Teste de adicao em lista vazia
	 * 
	 */
	@Test
	void testInsertFirst1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		/* Execucao */

		contas.insertFirst(cc1);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
	}

	/**
	 * Teste de adicao em lista com unico elemento
	 * 
	 */
	@Test
	void testInsertFirst2() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		/* Execucao */

		contas.insertFirst(cc1);
		contas.insertFirst(cc2); // elemento adicionado no inicio da lista

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto()); // verificando elemento do inicio da lista
		assertEquals(cc1, contas.getFim().getObjeto()); // verificando elemento do fim da lista
	}

	/**
	 * Teste de adicao em lista com multiplos elementos
	 * 
	 */
	@Test
	void testInsertFirst3() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		/* Execucao */

		contas.insertFirst(cc1);
		contas.insertFirst(cc2);
		contas.insertFirst(cc3);
		contas.insertFirst(cc4);

		/* Verificacao */

		int tamanhoAtual = 4;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc4, contas.getInicio().getObjeto()); // verificando elemento do inicio da lista
		assertEquals(cc1, contas.getFim().getObjeto()); // verificando elemento do fim da lista
	}

	/**
	 * Teste de Exception de adicao de elemento nulo
	 * 
	 */
	@Test
	void testInsertFirstException1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		int tamanhoAtual = 0;

		assertThrows(IllegalArgumentException.class, () -> {
			contas.insertFirst(null);
		});
		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
	}

	/**
	 * Teste de adicao em lista vazia
	 * 
	 */
	@Test
	void testInsertLast1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		/* Execucao */

		contas.insertFirst(cc1);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getFim().getObjeto());
	}

	/**
	 * Teste de adicao em lista com um unico elemento
	 * 
	 */
	@Test
	void testInsertLast2() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		/* Execucao */

		contas.insertLast(cc1);
		contas.insertLast(cc2); // elemento adicionado no fim da lista

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc2, contas.getFim().getObjeto());
	}

	/**
	 * Teste de adicao em lista com multiplos elementos
	 * 
	 */
	@Test
	void testInsertLast3() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		/* Execucao */

		contas.insertLast(cc1);
		contas.insertLast(cc2);
		contas.insertLast(cc3);
		contas.insertLast(cc4);

		/* Verificacao */

		int tamanhoAtual = 4;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto()); // verificando elemento do inicio da lista
		assertEquals(cc4, contas.getFim().getObjeto()); // verificando elemento do fim da lista
	}

	/**
	 * Teste de Exception de adicao de elemento nulo
	 * 
	 */
	@Test
	void testInsertLastException1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		int tamanhoAtual = 0;

		assertThrows(IllegalArgumentException.class, () -> {
			contas.insertLast(null);
		});
		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
	}

	/**
	 * Teste de remocao de elemento em lista com unico elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveFirst1() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Execucao */

		contas.removeFirst();

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
	}

	/**
	 * Teste de remocao de elemento em lista com unico elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveFirst2() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertLast(cc1);
		contas.insertLast(cc2); // elemento adicionado no fim da lista

		/* Execucao */

		contas.removeFirst();

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());
	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveFirst3() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1); // fim da fila
		contas.insertFirst(cc2);
		contas.insertFirst(cc3); // inicio da fila apos remocao de elemento
		contas.insertFirst(cc4); // elemento a ser removido

		/* Execucao */

		contas.removeFirst();

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc3, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());
	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos inseridos com
	 * metodos alternados
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveFirst4() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1);
		contas.insertLast(cc2);
		contas.insertFirst(cc3);
		contas.insertLast(cc4);

		/* Execucao */

		contas.removeFirst();

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc4, contas.getFim().getObjeto());
	}

	/**
	 * Teste de duas remocoes de elementos em lista com multiplos elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveFirst5() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1); // fim da fila
		contas.insertFirst(cc2); // inicio da fila apos remocao de elemento
		contas.insertFirst(cc3); // elemento a ser removido
		contas.insertFirst(cc4); // elemento a ser removido

		/* Execucao */

		contas.removeFirst();
		contas.removeFirst();

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());
	}

	/**
	 * Teste de Exception de adicao de elemento nulo
	 * 
	 */
	@Test
	void testRemoveFirstException1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());

		assertThrows(ListaVaziaException.class, () -> {
			contas.removeFirst();
		});

	}

	/**
	 * Teste de remocao de elemento em lista com unico elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveLast1() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Execucao */

		contas.removeLast();

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
	}

	/**
	 * Teste de remocao de elemento em lista com unico elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveLast2() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertLast(cc1);
		contas.insertLast(cc2);

		/* Execucao */

		contas.removeLast();

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());
	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveLast3() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1); // elemento a ser removido
		contas.insertFirst(cc2);
		contas.insertFirst(cc3);
		contas.insertFirst(cc4);

		/* Execucao */

		contas.removeLast();

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc4, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());
	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos inseridos com
	 * metodos alternados
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveLast4() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1);
		contas.insertLast(cc2); // fim da fila
		contas.insertFirst(cc3); // inicio da fila
		contas.insertLast(cc4); // elemento a ser removido

		/* Execucao */

		contas.removeLast();

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc3, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());
	}

	/**
	 * Teste de duas remocoes de elementos em lista com multiplos elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveLast5() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1); // elemento a ser removido
		contas.insertFirst(cc2); // elemento a ser removido
		contas.insertFirst(cc3); // fim da fila
		contas.insertFirst(cc4); // inicio da fila apos remocao de elemento

		/* Execucao */

		contas.removeLast();
		contas.removeLast();

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc4, contas.getInicio().getObjeto());
		assertEquals(cc3, contas.getFim().getObjeto());
	}

	/**
	 * Teste de Exception de adicao de elemento nulo
	 * 
	 */
	@Test
	void testRemoveLastException1() {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());

		assertThrows(ListaVaziaException.class, () -> {
			contas.removeLast();
		});

	}

	/**
	 * Teste de remocao de elemento por valor igual ao unico elemento da lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValue1() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Execucao */

		contas.removeByValue(cc1);

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
		assertEquals(null, contas.getFim());

	}

	/**
	 * Teste de remocao de elemento sendo ele o primeiro da lista de 2 elementos
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValue2() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertFirst(cc1);
		contas.insertFirst(cc2);

		/* Execucao */

		contas.removeByValue(cc2);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento sendo ele o ultimo da lista de 2 elementos
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValue3() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertFirst(cc1);
		contas.insertLast(cc2);

		/* Execucao */

		contas.removeByValue(cc2);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento sendo ele o elemento do meio da lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValue4() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");

		contas.insertLast(cc1); // inicio da lista
		contas.insertLast(cc2); // elemento do meio a ser removido
		contas.insertLast(cc3); // fim da lista

		/* Execucao */

		contas.removeByValue(cc2);

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc3, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos inseridos com
	 * metodos alternados
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveByValue5() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1);
		contas.insertLast(cc2); // elemento a ser removido
		contas.insertFirst(cc3); // inicio da fila apos remocao
		contas.insertLast(cc4); // fim da fila apos remocao

		/* Execucao */

		contas.removeByValue(cc2);

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc3, contas.getInicio().getObjeto());
		assertEquals(cc4, contas.getFim().getObjeto());
	}

	/**
	 * Teste Exception de remocao de elemento inexistente na lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValueException1() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertFirst(cc1);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertThrows(ElementoNaoEncontradoException.class, () -> {
			contas.removeByValue(cc2);
		});
	}

	/**
	 * Teste Exception de remocao de elemento em lista vazia
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValueException2() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
		assertThrows(ListaVaziaException.class, () -> {
			contas.removeByValue(cc1);
		});
	}

	/**
	 * Teste Exception de remocao de elemento nulo
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByValueException3() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertThrows(IllegalArgumentException.class, () -> {
			contas.removeByValue(null);
		});
	}

	/* A PARTIR DAQUIIIIIIIIIIII DAQUIIIIIIIIIIIIIIIIIIIIII */

	/**
	 * Teste de remocao de elemento por index igual ao unico elemento da lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndex1() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Execucao */

		int index = 0; // index do primeiro e unico elemento da lista

		contas.removeByIndex(index);

		/* Verificacao */

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
		assertEquals(null, contas.getFim());

	}

	/**
	 * Teste de remocao de elemento sendo ele o primeiro da lista de 2 elementos
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndex2() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertFirst(cc1); // index 0
		contas.insertFirst(cc2);

		/* Execucao */

		int index = 0; // index do primeiro elemento da lista

		contas.removeByIndex(index);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento sendo ele o ultimo da lista de 2 elementos
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndex3() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.insertFirst(cc1); // index 1
		contas.insertFirst(cc2);

		/* Execucao */

		int index = 1; // index do ultimo elemento da lista

		contas.removeByIndex(index);

		/* Verificacao */

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento do meio da lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndex4() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");

		contas.insertLast(cc1); // inicio da lista
		contas.insertLast(cc2); // index 1
		contas.insertLast(cc3); // fim da lista apos remocao

		/* Execucao */

		int index = 1; // index do elemento do meio da lista

		contas.removeByIndex(index);

		/* Verificacao */

		int tamanhoAtual = 2;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc3, contas.getFim().getObjeto());

	}

	/**
	 * Teste de remocao de elemento em lista com multiplos elementos inseridos com
	 * metodos alternados
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testRemoveByIndex5() throws ListaVaziaException, ElementoNaoEncontradoException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.insertFirst(cc1);
		contas.insertLast(cc2); // elemento a ser removido
		contas.insertFirst(cc3); // inicio da fila apos remocao
		contas.insertLast(cc4); // fim da fila apos remocao

		/* Execucao */

		int index = 1; // index do elemento e ser removido

		contas.removeByIndex(index);

		/* Verificacao */

		int tamanhoAtual = 3;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc3, contas.getInicio().getObjeto());
		assertEquals(cc4, contas.getFim().getObjeto());
	}

	/**
	 * Teste Exception de index uma posicao fora lista
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndexException1() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1); // index 0

		/* Verificacao */

		int index = 1; // index fora da lista

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());

		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			contas.removeByIndex(index);
		});
	}

	/**
	 * Teste Exception de remocao com index negativo
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndexException2() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.insertFirst(cc1);

		/* Verificacao */

		int index = -1; // index com valor negativo

		int tamanhoAtual = 1;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertThrows(IllegalArgumentException.class, () -> {
			contas.removeByIndex(index);
		});
	}

	/**
	 * Teste Exception de remocao de elemento em lista vazia
	 * 
	 * @throws ListaVaziaException
	 * @throws ElementoNaoEncontradoException
	 * 
	 */
	@Test
	void testRemoveByIndexException3() throws ListaVaziaException {

		/* Cenario */

		Deque<Conta> contas = new Deque<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		int index = 0;

		int tamanhoAtual = 0;

		assertEquals(tamanhoAtual, contas.size());
		assertEquals(null, contas.getInicio());
		assertThrows(ListaVaziaException.class, () -> {
			contas.removeByIndex(index);
		});
	}

}
