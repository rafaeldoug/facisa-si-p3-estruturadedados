/**
 * 
 */
package br.cesed.si.p3.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.Pilha;
import br.cesed.si.p3.entities.Conta;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
class PilhaTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Teste de adicao de elemento em lista vazia
	 */
	@Test
	void testPush1() {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		/* Execucao */

		contas.push(cc1);

		/* Verificacao */

		assertFalse(contas.isEmpty());
		assertEquals(cc1, contas.getInicio().getObjeto());
	}

	/**
	 * Testa o push de varios elementos
	 */
	@Test
	void testPush2() {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		/* Execucao */

		contas.push(cc1);
		contas.push(cc2);
		contas.push(cc3);
		contas.push(cc4); // topo da Pilha

		/* Verificacao */

		int tamanhoEsperado = 4;

		assertFalse(contas.isEmpty());
		assertEquals(tamanhoEsperado, contas.size()); // confere o tamanho da fila
		assertEquals(cc1, contas.getInicio().getObjeto()); // confere o primeiro elemento da pilha
		assertEquals(cc4, contas.getFim().getObjeto()); // confere o ultimo elemento da pilha

	}

	/**
	 * Teste de adicao de elemento nulo
	 */
	@Test
	void testPushException1() {

		/* Cenario */

		Pilha<?> contas = new Pilha<Object>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			contas.push(null);
		});

	}

	/**
	 * Teste de remocao de elemento de Pilha com apenas um elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testPop1() throws ListaVaziaException {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.push(cc1);

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.pop();

		/* Verificacao */

		int tamanhoEsperado = 0;

		assertTrue(contas.isEmpty());
		assertEquals(tamanhoEsperado, contas.size());

	}

	/**
	 * Teste de remocao de elemento de Pilha com varios elemento
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testPop2() throws ListaVaziaException {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.push(cc1);
		contas.push(cc2);
		contas.push(cc3); // elemento que sera novo topo da pilha
		contas.push(cc4); // elemento que sera eliminado da pilha

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.pop();

		/* Verificacao */

		int tamanhoEsperado = 3;

		assertEquals(tamanhoEsperado, contas.size());
		assertEquals(cc3, contas.getFim().getObjeto()); // confere o novo topo da pilha
		assertEquals(null, contas.getFim().getProxNode());

	}

	/**
	 * Teste de topo da lista com um elemento
	 */
	@Test
	void testTop1() {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.push(cc1);

		/* Verificacao */

		assertEquals(cc1, contas.top());
	}

	/**
	 * Teste de topo da lista com varios elementos
	 */
	@Test
	void testTop2() {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.push(cc1);
		contas.push(cc2);
		contas.push(cc3);
		contas.push(cc4); // elemento no topo da lista

		assertFalse(contas.isEmpty());

		/* Verificacao */

		assertEquals(cc4, contas.top()); // confere o elemento no topo da lista
	}
	
	/**
	 * Teste de adicao de elemento em lista vazia
	 */
	@Test
	void testClear1() {

		/* Cenario */

		Pilha<Conta> contas = new Pilha<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		
		contas.push(cc1);

		/* Execucao */

		contas.clear();

		/* Verificacao */

		assertTrue(contas.isEmpty());
		assertEquals(0, contas.size());
		assertEquals(null, contas.getInicio());
		assertEquals(null, contas.getFim());
	}


}
