/**
 * 
 */
package br.cesed.si.p3.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.ListaDuplamenteEncadeada;
import br.cesed.si.p3.entities.Conta;
import br.cesed.si.p3.exceptions.ElementoNaoEncontradoException;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
class ListaDuplamenteEncadeadaTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 */
	@Test
	void testAdd1() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		/* Execucao */

		contas.add(cc1); // elemento adicionado

		/* Verificacao */

		int tamanhoAtual = 1; // tamanho atual da lista

		assertFalse(contas.isEmpty()); // assegura que a lista nao esta vazia
		assertEquals(tamanhoAtual, contas.size()); // confere o tamanho atual da lista
		assertEquals(cc1, contas.getInicio().getObjeto()); // confere o elemento adicionado
	}

	/**
	 * 
	 */
	@Test
	void testAdd2() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		/* Execucao */

		contas.add(cc1);
		contas.add(cc2);

		/* Verificacao */

		assertEquals(2, contas.size()); // confere o tamanho da lista
		assertEquals(cc2, contas.getInicio().getProxNode().getObjeto()); // confere o segundo elemento da lista

	}

	/**
	 * 
	 */
	@Test
	void testAdd3() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		/* Execucao */

		contas.add(cc1);
		contas.add(cc2);
		contas.add(cc3); // elemento a ser conferido
		contas.add(cc4);

		/* Verificacao */

		assertEquals(4, contas.size());
		assertEquals(cc3, contas.getFim().getAntNode().getObjeto()); // confere o segundo elemento da lista

	}

	/**
	 * 
	 */
	@Test
	void testAddAll1() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");
		Conta cc5 = new Conta(005, 150, "Jounes");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();
		contas.add(cc1);
		contas.add(cc2);

		ListaDuplamenteEncadeada<Conta> contas2 = new ListaDuplamenteEncadeada<Conta>();
		contas2.add(cc3);
		contas2.add(cc4);
		contas2.add(cc5); // ultimo elemento da lista concatenada

		/* Execucao */

		contas.addAll(contas2);

		/* Verificacao */

		assertEquals(5, contas.size());
		assertEquals(cc5, contas.getFim().getObjeto()); // confere o ultimo elemento da lista

	}

	/**
	 * Testa o lancamento de Excecao ao adicionar elemento nulo
	 */
	@Test
	void testAddException1() {

		/* Cenario */

		ListaDuplamenteEncadeada<?> contas = new ListaDuplamenteEncadeada<Object>();

		/* Verificacao */

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			contas.add(null);
		});
	}
	
	/**
	 * Remove elemento em lista de elemento unico
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	@Test
	void testRemove1() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);

		assertEquals(1, contas.size());

		/* Execucao */

		contas.remove(cc1);

		/* Verificacao */

		assertEquals(0, contas.size());
		assertEquals(null, contas.getInicio());
	}


	/**
	 * Remove o elemento no inicio da lista com multiplos elementos
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	@Test
	void testRemove2() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);
		contas.add(cc3);
		contas.add(cc4);

		assertEquals(4, contas.size());

		/* Execucao */

		contas.remove(cc1);

		/* Verificacao */

		assertEquals(3, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
	}

	/**
	 * Remove elemento do meio da lista de multiplos elementos
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	@Test
	void testRemove3() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");
		Conta cc5 = new Conta(005, 150, "Uno");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);
		contas.add(cc3);
		contas.add(cc4);
		contas.add(cc5);

		assertEquals(5, contas.size());

		/* Execucao */

		contas.remove(cc3);

		/* Verificacao */

		assertEquals(4, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc5, contas.getFim().getObjeto());
	}
	
	/**
	 * Remove elemento do fim da lista de 2 elementos
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	@Test
	void testRemove4() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);

		assertEquals(2, contas.size());

		/* Execucao */

		contas.remove(cc2);

		/* Verificacao */

		assertEquals(1, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());
		assertEquals(cc1, contas.getFim().getObjeto());
	}
	
	/**
	 * Remove elemento do inicio da lista de 2 elementos
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	@Test
	void testRemove5() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);

		assertEquals(2, contas.size());

		/* Execucao */

		contas.remove(cc1);

		/* Verificacao */

		assertEquals(1, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());
	}

	/**
	 * Teste de Exception com Elemento Nao Encontrado
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException 
	 */
	@Test
	void testRemoveException1() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);

		assertEquals(1, contas.size());

		/* Verificacao */

		assertThrows(ElementoNaoEncontradoException.class, () -> {
			contas.remove(cc2);
		});
		assertEquals(1, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());

	}
	
	/**
	 * Teste de Exception com Elemento removido anteriormente
	 * 
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException 
	 */
	@Test
	void testRemoveException2() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);

		assertEquals(2, contas.size());
		
		/* Execucao */
		
		contas.remove(cc2); // remocao do elemento

		/* Verificacao */

		assertThrows(ElementoNaoEncontradoException.class, () -> {
			contas.remove(cc2); 
		});
		assertEquals(1, contas.size());
		assertEquals(cc1, contas.getInicio().getObjeto());

	}
	
	/**
	 * Teste de Exception para Remove com Lista Vazia
	 * 
	 * @throws ElementoNaoEncontradoException
	 */
	@Test
	void testRemoveException3() throws ElementoNaoEncontradoException, ListaVaziaException {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		assertEquals(0, contas.size());

		/* Verificacao */

		assertThrows(ListaVaziaException.class, () -> {
			contas.remove(cc1);
		});

	}


	/**
	 * Teste de Index de elementos fora da lista
	 */
	@Test
	void testIndexOf1() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		assertEquals(1, contas.size());

		/* Execu��o */

		int posAtual = contas.indexOf(cc2);

		/* Verifica��o */

		assertEquals(-1, posAtual);

	}

	/**
	 * Teste de Index de elemento no inicio da lista
	 */
	@Test
	void testIndexOf2() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1); // elemnto com a posicao a ser buscada

		assertEquals(1, contas.size());

		/* Execu��o */

		int posAtual = contas.indexOf(cc1);

		/* Verifica��o */
		int posEsperada = 0; // posicao que o elemento deve estar

		assertEquals(posEsperada, posAtual);

	}

	/**
	 * Teste de Index de elemento no fim da lista
	 */
	@Test
	void testIndexOf3() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");
		Conta cc5 = new Conta(005, 150, "Uno");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);
		contas.add(cc3);
		contas.add(cc4);
		contas.add(cc5); // elemento com o posicao a ser buscada

		assertEquals(5, contas.size());

		/* Execu��o */

		int posAtual = contas.indexOf(cc5);

		/* Verifica��o */

		int posEsperada = 4; // posicao que o elemento deve estar
		assertEquals(posEsperada, posAtual);

	}

	/**
	 * Teste retorno de elemento na posi��o indicada
	 */
	@Test
	void testGetElemento1() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		assertTrue(contas.isEmpty());

		contas.add(cc1);

		/* Execu��o */

		int posAtual = 0;
		/* Verifica��o */

		assertEquals(cc1, contas.getElemento(posAtual));

	}

	/**
	 * Teste de Ajuste de elemento
	 */
	@Test
	void testSetElemento1() {

		/* Cenario */

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");
		Conta cc5 = new Conta(005, 150, "Uno"); // elemento que sera ajustado na lista

		ListaDuplamenteEncadeada<Conta> contas = new ListaDuplamenteEncadeada<Conta>();

		contas.add(cc1);
		contas.add(cc2);
		contas.add(cc3); // posicao que recebera o elemento
		contas.add(cc4);

		assertEquals(4, contas.size());

		/* Execu��o */

		contas.setElemento(2, cc5);

		/* Verifica��o */

		assertEquals(cc5, contas.getElemento(2)); // o elemento deve constar na posicao 2
		assertEquals(-1, contas.indexOf(cc3)); // o elemento substituido nao deve estar contido na lista

	}

	/**
	 * 
	 */
	@Test
	void testIsEmpty() {

		/* Cen�rio */

		ListaDuplamenteEncadeada<?> contas = new ListaDuplamenteEncadeada<Object>();

		/* Verifica��o */

		assertTrue(contas.isEmpty());
		assertEquals(0, contas.size());
	}

}
