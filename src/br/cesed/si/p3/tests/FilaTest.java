/**
 * 
 */
package br.cesed.si.p3.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.Fila;
import br.cesed.si.p3.entities.Conta;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
class FilaTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Teste de enqueue de um elemento em lista vazia
	 */
	@Test
	void testEnqueue1() {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		/* Execucao */

		contas.enqueue(cc1);

		/* Verificacao */

		assertFalse(contas.isEmpty());
		assertEquals(cc1, contas.getInicio().getObjeto());

	}

	/**
	 * Testa o enqueue de varios elementos
	 */
	@Test
	void testEnqueue2() {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		/* Execucao */

		contas.enqueue(cc1); // inicio da fila
		contas.enqueue(cc2);
		contas.enqueue(cc3);
		contas.enqueue(cc4); // fim da fila

		/* Verificacao */

		int tamanhoEsperado = 4;

		assertFalse(contas.isEmpty());
		assertEquals(tamanhoEsperado, contas.size()); // confere o tamanho da fila
		assertEquals(cc1, contas.getInicio().getObjeto()); // confere o primeiro elemento da fila
		assertEquals(cc4, contas.getFim().getObjeto()); // confere o ultimo elemento da fila

	}

	/**
	 * Testa o enqueue de um elemento em lista tamanho 1
	 */
	@Test
	void testEnqueue3() {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		/* Execucao */

		contas.enqueue(cc1); // inicio da fila
		contas.enqueue(cc2); // fim da fila

		/* Verificacao */

		int tamanhoEsperado = 2;

		assertFalse(contas.isEmpty());
		assertEquals(tamanhoEsperado, contas.size()); // confere o tamanho da fila
		assertEquals(cc1, contas.getInicio().getObjeto()); // confere o primeiro elemento da fila
		assertEquals(cc2, contas.getFim().getObjeto()); // confere o ultimo elemento da fila

	}

	/**
	 * Teste de adicao de elemento nulo
	 */
	@Test
	void testEnqueueException1() {

		/* Cenario */

		Fila<?> contas = new Fila<Object>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			contas.enqueue(null);
		});

	}

	/**
	 * Testa remocao de elemento em lista de elemento unico
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testDequeue1() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.enqueue(cc1);

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.dequeue();

		/* Verificacao */

		int tamanhoEsperado = 0;

		assertTrue(contas.isEmpty());
		assertEquals(tamanhoEsperado, contas.size());
		assertEquals(null, contas.getInicio());

	}

	/**
	 * Testa remocao de elemento em lista com 2 elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testDequeue2() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");

		contas.enqueue(cc1);
		contas.enqueue(cc2);

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.dequeue();

		/* Verificacao */

		int tamanhoEsperado = 1;

		assertEquals(tamanhoEsperado, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc2, contas.getFim().getObjeto());

	}

	/**
	 * Testa remocao de elemento em lista de multiplos elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testDequeue3() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.enqueue(cc1);
		contas.enqueue(cc2); // inicio da fila apos remocao
		contas.enqueue(cc3);
		contas.enqueue(cc4);

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.dequeue();

		/* Verificacao */

		int tamanhoEsperado = 3;

		assertEquals(tamanhoEsperado, contas.size());
		assertEquals(cc2, contas.getInicio().getObjeto());
		assertEquals(cc4, contas.getFim().getObjeto());

	}

	/**
	 * Testa remocao de dois elementos
	 * 
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testDequeue4() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.enqueue(cc1);
		contas.enqueue(cc2);
		contas.enqueue(cc3); // inicio da fila apos remocao
		contas.enqueue(cc4);

		assertFalse(contas.isEmpty());

		/* Execucao */

		contas.dequeue();
		contas.dequeue();

		/* Verificacao */

		int tamanhoEsperado = 2;

		assertEquals(tamanhoEsperado, contas.size());
		assertEquals(cc3, contas.getInicio().getObjeto());
		assertEquals(cc4, contas.getFim().getObjeto());

	}

	/**
	 * @throws ListaVaziaException
	 * 
	 */
	@Test
	void testDequeueException1() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		/* Verificacao */

		assertThrows(ListaVaziaException.class, () -> {
			contas.dequeue();
		});

	}

	/**
	 * Teste do metodo front em lista com um elemento
	 * 
	 * @throws ListaVaziaException
	 */
	@Test
	void testFront1() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");

		contas.enqueue(cc1);

		/* Verificacao */

		assertFalse(contas.isEmpty());
		assertEquals(cc1, contas.front());

	}
	

	/**
	 * Teste do metodo front em lista com varios elementos
	 * 
	 * @throws ListaVaziaException
	 */
	@Test
	void testFront2() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.enqueue(cc1);
		contas.enqueue(cc2);
		contas.enqueue(cc3);
		contas.enqueue(cc4);

		/* Verificacao */

		assertFalse(contas.isEmpty());
		assertEquals(cc1, contas.front());

	}
	
	/**
	 * Teste do metodo front com remocao de varios elementos
	 * 
	 * @throws ListaVaziaException
	 */
	@Test
	void testFront3() throws ListaVaziaException {

		/* Cenario */

		Fila<Conta> contas = new Fila<Conta>();

		assertTrue(contas.isEmpty());

		Conta cc1 = new Conta(001, 110, "Lailo");
		Conta cc2 = new Conta(002, 120, "Sabrino");
		Conta cc3 = new Conta(003, 130, "Danielo");
		Conta cc4 = new Conta(004, 140, "Josefo");

		contas.enqueue(cc1);
		contas.enqueue(cc2);
		contas.enqueue(cc3);
		contas.enqueue(cc4);

		/* Verificacao */

		assertEquals(cc1, contas.front());
		contas.dequeue();
		assertEquals(cc2, contas.front());
		contas.dequeue();
		assertEquals(cc3, contas.front());
		contas.dequeue();
		assertEquals(cc4, contas.front());

	}

}
