/**
 * 
 */
package br.cesed.si.p3;

import br.cesed.si.p3.exceptions.ElementoNaoEncontradoException;
import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 * 
 */
public class ListaDuplamenteEncadeada<T> extends Lista {

	private T objeto;

	/**
	 * Construtor de lista vazia
	 */
	public ListaDuplamenteEncadeada() {
	}

	/**
	 * Construtor de lista contendo elemento
	 * 
	 * @param objeto - Elemento a ser adicionado na lista
	 */
	public ListaDuplamenteEncadeada(T objeto) {
		this.objeto = objeto;
	}

	/**
	 * Adiciona um elemento na lista
	 * 
	 * @param objeto - Elemento a ser adicionado na lista
	 */
	public void add(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel adicionar valores nulos.");
		}

		if (isEmpty()) {
			inicio = fim = new Node(objeto);
		} else {
			Node aux = inicio;
			while (aux.getProxNode() != null) {
				aux = aux.getProxNode();
			}
			fim = new Node(objeto);
			fim.setAntNode(aux);
			aux.setProxNode(fim);

		}
		inseridos++;

	}

	/**
	 * @param objeto
	 */
	public void addAll(ListaDuplamenteEncadeada<T> lista) {

		if (lista == null) {
			throw new IllegalArgumentException();
		} else {
			inicio.setAntNode(fim);
			fim.setProxNode(lista.inicio);
			fim = lista.fim;
		}
		inseridos += lista.inseridos;
	}

	/**
	 * Remove elemento da lista
	 * 
	 * @param objeto - Elemento a ser removido da lista
	 * @throws ElementoNaoEncontradoException
	 * @throws ListaVaziaException
	 */
	public void remove(T objeto) throws ElementoNaoEncontradoException, ListaVaziaException {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel remover valores nulos.");
		}

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		if (inicio.getObjeto().equals(objeto)) {

			inicio = inicio.getProxNode();

		} else if(size() == 2 && fim.getObjeto().equals(objeto)) {
			
			inicio.setProxNode(null);
			fim = inicio;
			
		} else {

			boolean encontrado = false;
			Node aux = inicio;
			Node atual = aux;

			for (int i = 0; i < inseridos && encontrado == false; i++) {
				if (aux.getObjeto().equals(objeto)) {
					encontrado = true;
				}
				atual = aux;
				aux = aux.getProxNode();
			}

			if (encontrado == false) {
				throw new ElementoNaoEncontradoException("Elemento n�o encontrado!");
			} else {
				atual.setProxNode(aux.getProxNode());
				atual.getProxNode().setAntNode(atual);
				aux.setAntNode(null);
				aux.setProxNode(null);
			}
		}
		inseridos--;
	}

	/**
	 * Remove elemento da lista na posicao indicada
	 * 
	 * @param index - Posicao do elemento a ser removido
	 */
	public void removeIndex(int index) {

		if (inseridos < index) {
			throw new ArrayIndexOutOfBoundsException("Insira uma posi��o v�lida!");
		} else if (index < 0) {
			throw new NumberFormatException("N�o s�o permitios valores negativos!");
		}

		if (index == 0) {
			inicio = inicio.getProxNode();
		} else {
			Node aux = inicio.getProxNode();
			Node atual = aux;
			int pos = 1;
			while (pos < index) {
				atual = aux;
				aux = aux.getProxNode();
				pos++;
			}
			atual.setProxNode(aux.getProxNode());
			atual.getProxNode().setAntNode(atual);
			aux.setAntNode(null);
			aux.setProxNode(null);
		}
		inseridos--;
	}

	/**
	 * Retorna a posicao do elemento indicado
	 * 
	 * @param objeto - Elemento a ser buscado a posicao
	 * @return Retorna a posicao do elemento buscado
	 */
	public int indexOf(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel buscar valores nulos.");
		}

		Node aux = inicio;
		int pos = -1;
		int posInicial = 0;
		if (inicio.getObjeto().equals(objeto)) {
			pos = posInicial;
		} else {
			aux = aux.getProxNode();
			posInicial++;
			while (aux != null) {
				if (aux.getObjeto().equals(objeto)) {
					pos = posInicial;
					break;
				}
				aux = aux.getProxNode();
				posInicial++;
			}
		}
		return pos;

	}

	/**
	 * Retorna o elemento na posicao indicada
	 *
	 * @param index - Posicao que deseja retornar o elemento
	 * @return Retorna o objeto na posicao indicada
	 */
	public Object getElemento(int index) {

		if (inseridos < index) {
			throw new ArrayIndexOutOfBoundsException("Insira uma posi��o v�lida!");
		} else if (index < 0) {
			throw new NumberFormatException("N�o s�o permitios valores negativos!");
		}

		Node aux = null;
		if (index <= (inseridos / 2)) {
			aux = inicio;
			int pos = 0;
			while (pos < index) {
				aux = aux.getProxNode();
				pos++;
			}
		} else {
			aux = fim;
			int pos = inseridos - 1;
			while (pos > index) {
				aux = aux.getAntNode();
				pos--;
			}
		}
		return aux.getObjeto();
	}

	/**
	 * Ajusta o elemento na posicao indicada
	 * 
	 * @param index  - Posicao do elemento que deseja alterar
	 * @param objeto - Elemento que sera ajustado a posicao indicada
	 */
	public void setElemento(int index, T objeto) {

		if (inseridos < index) {
			throw new ArrayIndexOutOfBoundsException("Insira uma posi��o v�lida!");
		} else if (index < 0) {
			throw new NumberFormatException("N�o s�o permitios valores negativos!");
		}

		Node aux = null;
		if (index <= (inseridos / 2)) {
			aux = inicio;
			int pos = 0;
			while (pos < index) {
				aux = aux.getProxNode();
				pos++;
			}
		} else {
			aux = fim;
			int pos = inseridos - 1;
			while (pos > index) {
				aux = aux.getAntNode();
				pos--;
			}
		}
		aux.setObjeto(objeto);
	}

	/**
	 * Retorna o objeto
	 * 
	 * @return the objeto
	 */
	public T getObjeto() {
		return objeto;
	}

}
