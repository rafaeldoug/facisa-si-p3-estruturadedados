/**
 * 
 */
package br.cesed.si.p3;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
public class Node {

	private Object objeto; // Recebe o objeto do no
	private Node antNode; // Recebe o No anterior
	private Node proxNode; // Recebe o No posterior

	/**
	 * Construtor que recebe o objeto do no
	 * 
	 * @param objeto - Recebe o objeto no
	 */
	public Node(Object objeto) {
		this.objeto = objeto;
	}

	/**
	 * Construtor que recebe o objeto do no e o proximo no
	 * 
	 * @param objeto   - Recebe objeto do no
	 * @param proxNode - Recebe o proximo no
	 */
	public Node(Object objeto, Node proxNode) {
		this.objeto = objeto;
		this.proxNode = proxNode;
	}

	/**
	 * Construtor que recebe o objeto do no, do no anterior e do proximo no
	 * 
	 * @param objeto   - Recebe objeto do no
	 * @param antNode  - Recebe o no anterior
	 * @param proxNode - Recebe o proximo no
	 */
	public Node(Object objeto, Node antNode, Node proxNode) {
		super();
		this.objeto = objeto;
		this.antNode = antNode;
		this.proxNode = proxNode;
	}

	/**
	 * @return Retorna o objeto do no
	 */
	public Object getObjeto() {
		return objeto;
	}

	/**
	 * @param objeto the objeto to set
	 */
	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	/**
	 * @return Retorna o proximo objeto do no
	 */
	public Node getProxNode() {
		return proxNode;
	}

	/**
	 * Ajusta o proximo no do objeto
	 * 
	 * @param objeto - Referencia o proximo no
	 */
	public void setProxNode(Node proxNode) {
		this.proxNode = proxNode;
	}

	/**
	 * @return the antNode
	 */
	public Node getAntNode() {
		return antNode;
	}

	/**
	 * @param antNode the antNode to set
	 */
	public void setAntNode(Node antNode) {
		this.antNode = antNode;
	}

}
