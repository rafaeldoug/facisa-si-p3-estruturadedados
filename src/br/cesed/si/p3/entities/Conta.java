package br.cesed.si.p3.entities;

public class Conta {

	private int numero;

	private int agencia;

	private double saldo;

	private String nome;

	/**
	 * @param numero
	 * @param agencia
	 * @param nome
	 */
	public Conta(int numero, int agencia, String nome) {
		super();
		this.numero = numero;
		this.agencia = agencia;
		this.nome = nome;
	}

	/**
	 * @param numero
	 * @param agencia
	 * @param saldo
	 * @param nome
	 */
	public Conta(int numero, int agencia, double saldo, String nome) {
		super();
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
		this.nome = nome;
	}

	/**
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * @return the agencia
	 */
	public int getAgencia() {
		return agencia;
	}

	/**
	 * @param agencia the agencia to set
	 */
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the saldo
	 */
	public double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "*** Conta n.: " + numero + " Agencia = " + agencia + " ***" + "\n| Nome = " + nome + "|"
				+ "\n| SALDO = " + saldo + "|";
	}

}
