package br.cesed.si.p3;

import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 * 
 */

public abstract class Lista {

	protected Node inicio;
	protected Node fim;
	protected int inseridos;

	/**
	 * @return
	 */
	public int size() {
		return inseridos;
	}

	/**
	 * @throws ListaVaziaException
	 * 
	 */
	public void listar() throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Lista vazia!");
		} else {
			Node aux = inicio;
			while (aux.getProxNode() != null) {
				System.out.println(aux.getObjeto());
				aux = aux.getProxNode();
			}
			System.out.println(aux.getObjeto());
		}

	}

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return inicio == null;
	}

	/**
	 * @return the inicio
	 */
	public Node getInicio() {
		return inicio;
	}

	/**
	 * @return the fim
	 */
	public Node getFim() {
		return fim;
	}

	/**
	 * @return the inseridos
	 */
	public int getInseridos() {
		return inseridos;
	}

}
