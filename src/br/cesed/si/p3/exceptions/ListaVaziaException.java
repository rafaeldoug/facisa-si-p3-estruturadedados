package br.cesed.si.p3.exceptions;

public class ListaVaziaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public ListaVaziaException(String message) {
		super(message);
	}
	

}
