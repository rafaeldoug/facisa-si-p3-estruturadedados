package br.cesed.si.p3.exceptions;

public class ElementoNaoEncontradoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ElementoNaoEncontradoException(String message) {
		super(message);
	}

}
