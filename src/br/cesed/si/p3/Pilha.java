/**
 * 
 */
package br.cesed.si.p3;

import br.cesed.si.p3.exceptions.ListaVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
public class Pilha<T> extends Lista {

	private T objeto;

	/**
	 * 
	 */
	public Pilha() {
	}

	/**
	 * @param objeto
	 */
	public Pilha(T objeto) {
		this.objeto = objeto;
	}

	public void push(T objeto) {

		if (objeto == null) {
			throw new IllegalArgumentException("N�o � poss�vel adicionar valores nulos.");
		}

		if (isEmpty()) {
			inicio = fim = new Node(objeto);
		} else {
			Node aux = fim;
			fim = new Node(objeto);
			aux.setProxNode(fim);
		}

		inseridos++;
	}

	public void pop() throws ListaVaziaException {

		if (isEmpty()) {
			throw new ListaVaziaException("Nao foi possivel remover elemento. Lista vazia!");
		}

		if (inseridos == 1) {
			inicio = fim = null;
			inseridos--;
		} else {
			Node aux = inicio;
			Node atual = aux;
			while(aux.getProxNode() != null) {
				atual = aux;
				aux = aux.getProxNode();
			}
			fim = atual;
			fim.setProxNode(null);
			inseridos--;
		}
	}

	public Object top() {
		
		return fim.getObjeto();

	}

	public void clear() {
	
		inicio = fim = null;
		inseridos = 0;

	}

	/**
	 * @return the objeto
	 */
	public Object getObjeto() {
		return objeto;
	}

}
